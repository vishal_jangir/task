class CreateTasks < ActiveRecord::Migration[5.2]
  def change
    create_table :tasks do |t|
      t.string :name
      t.integer :contact
      t.string :area_line
      t.string :address_line
      t.integer :service_type_id
      t.string :service_info
      t.integer :assigned_employee_id
      t.string :status
      t.string :priority
      t.string :billing
      t.string :payment_status
      t.string :remark

      t.timestamps
    end
  end
end
