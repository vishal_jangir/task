class CreateAssignedEmployees < ActiveRecord::Migration[5.2]
  def change
    create_table :assigned_employees do |t|
      t.string :name

      t.timestamps
    end
  end
end
