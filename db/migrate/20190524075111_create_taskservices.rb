class CreateTaskservices < ActiveRecord::Migration[5.2]
  def change
    create_table :taskservices do |t|
      t.integer :task_id
      t.integer :service_type_id

      t.timestamps
    end
  end
end
