Rails.application.routes.draw do
  resources :tasks
  resources :assigned_employees
  resources :service_types
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
