class AssignedEmployeesController < ApplicationController
  before_action :set_assigned_employee, only: [:show, :edit, :update, :destroy]

  # GET /assigned_employees
  # GET /assigned_employees.json
  def index
    @assigned_employees = AssignedEmployee.all
  end

  # GET /assigned_employees/1
  # GET /assigned_employees/1.json
  def show
  end

  # GET /assigned_employees/new
  def new
    @assigned_employee = AssignedEmployee.new
  end

  # GET /assigned_employees/1/edit
  def edit
  end

  # POST /assigned_employees
  # POST /assigned_employees.json
  def create
    @assigned_employee = AssignedEmployee.new(assigned_employee_params)

    respond_to do |format|
      if @assigned_employee.save
        format.html { redirect_to @assigned_employee, notice: 'Assigned employee was successfully created.' }
        format.json { render :show, status: :created, location: @assigned_employee }
      else
        format.html { render :new }
        format.json { render json: @assigned_employee.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /assigned_employees/1
  # PATCH/PUT /assigned_employees/1.json
  def update
    respond_to do |format|
      if @assigned_employee.update(assigned_employee_params)
        format.html { redirect_to @assigned_employee, notice: 'Assigned employee was successfully updated.' }
        format.json { render :show, status: :ok, location: @assigned_employee }
      else
        format.html { render :edit }
        format.json { render json: @assigned_employee.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /assigned_employees/1
  # DELETE /assigned_employees/1.json
  def destroy
    @assigned_employee.destroy
    respond_to do |format|
      format.html { redirect_to assigned_employees_url, notice: 'Assigned employee was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_assigned_employee
      @assigned_employee = AssignedEmployee.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def assigned_employee_params
      params.require(:assigned_employee).permit(:name)
    end
end
