json.extract! assigned_employee, :id, :name, :created_at, :updated_at
json.url assigned_employee_url(assigned_employee, format: :json)
