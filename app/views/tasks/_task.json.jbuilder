json.extract! task, :id, :name, :contact, :area_line, :address_line, :service_type_id, :service_info, :assigned_employee_id, :status, :priority, :billing, :payment_status, :remark, :created_at, :updated_at
json.url task_url(task, format: :json)
