class ServiceType < ApplicationRecord
	
	has_many :taskservices, dependent: :destroy
	has_many :tasks, :through => :taskservices
end
