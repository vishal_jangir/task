class Task < ApplicationRecord
	
	has_many :taskempoloyees, dependent: :destroy
	has_many :assigned_employees, :through => :taskempoloyees

	has_many :taskservices, dependent: :destroy
	has_many :service_types, :through => :taskservices
end
