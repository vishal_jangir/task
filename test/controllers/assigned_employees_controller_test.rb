require 'test_helper'

class AssignedEmployeesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @assigned_employee = assigned_employees(:one)
  end

  test "should get index" do
    get assigned_employees_url
    assert_response :success
  end

  test "should get new" do
    get new_assigned_employee_url
    assert_response :success
  end

  test "should create assigned_employee" do
    assert_difference('AssignedEmployee.count') do
      post assigned_employees_url, params: { assigned_employee: { name: @assigned_employee.name } }
    end

    assert_redirected_to assigned_employee_url(AssignedEmployee.last)
  end

  test "should show assigned_employee" do
    get assigned_employee_url(@assigned_employee)
    assert_response :success
  end

  test "should get edit" do
    get edit_assigned_employee_url(@assigned_employee)
    assert_response :success
  end

  test "should update assigned_employee" do
    patch assigned_employee_url(@assigned_employee), params: { assigned_employee: { name: @assigned_employee.name } }
    assert_redirected_to assigned_employee_url(@assigned_employee)
  end

  test "should destroy assigned_employee" do
    assert_difference('AssignedEmployee.count', -1) do
      delete assigned_employee_url(@assigned_employee)
    end

    assert_redirected_to assigned_employees_url
  end
end
