require 'test_helper'

class TasksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @task = tasks(:one)
  end

  test "should get index" do
    get tasks_url
    assert_response :success
  end

  test "should get new" do
    get new_task_url
    assert_response :success
  end

  test "should create task" do
    assert_difference('Task.count') do
      post tasks_url, params: { task: { address_line: @task.address_line, area_line: @task.area_line, assigned_employee_id: @task.assigned_employee_id, billing: @task.billing, contact: @task.contact, name: @task.name, payment_status: @task.payment_status, priority: @task.priority, remark: @task.remark, service_info: @task.service_info, service_type_id: @task.service_type_id, status: @task.status } }
    end

    assert_redirected_to task_url(Task.last)
  end

  test "should show task" do
    get task_url(@task)
    assert_response :success
  end

  test "should get edit" do
    get edit_task_url(@task)
    assert_response :success
  end

  test "should update task" do
    patch task_url(@task), params: { task: { address_line: @task.address_line, area_line: @task.area_line, assigned_employee_id: @task.assigned_employee_id, billing: @task.billing, contact: @task.contact, name: @task.name, payment_status: @task.payment_status, priority: @task.priority, remark: @task.remark, service_info: @task.service_info, service_type_id: @task.service_type_id, status: @task.status } }
    assert_redirected_to task_url(@task)
  end

  test "should destroy task" do
    assert_difference('Task.count', -1) do
      delete task_url(@task)
    end

    assert_redirected_to tasks_url
  end
end
