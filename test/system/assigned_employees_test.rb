require "application_system_test_case"

class AssignedEmployeesTest < ApplicationSystemTestCase
  setup do
    @assigned_employee = assigned_employees(:one)
  end

  test "visiting the index" do
    visit assigned_employees_url
    assert_selector "h1", text: "Assigned Employees"
  end

  test "creating a Assigned employee" do
    visit assigned_employees_url
    click_on "New Assigned Employee"

    fill_in "Name", with: @assigned_employee.name
    click_on "Create Assigned employee"

    assert_text "Assigned employee was successfully created"
    click_on "Back"
  end

  test "updating a Assigned employee" do
    visit assigned_employees_url
    click_on "Edit", match: :first

    fill_in "Name", with: @assigned_employee.name
    click_on "Update Assigned employee"

    assert_text "Assigned employee was successfully updated"
    click_on "Back"
  end

  test "destroying a Assigned employee" do
    visit assigned_employees_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Assigned employee was successfully destroyed"
  end
end
