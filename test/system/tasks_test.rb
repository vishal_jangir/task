require "application_system_test_case"

class TasksTest < ApplicationSystemTestCase
  setup do
    @task = tasks(:one)
  end

  test "visiting the index" do
    visit tasks_url
    assert_selector "h1", text: "Tasks"
  end

  test "creating a Task" do
    visit tasks_url
    click_on "New Task"

    fill_in "Address line", with: @task.address_line
    fill_in "Area line", with: @task.area_line
    fill_in "Assigned employee", with: @task.assigned_employee_id
    fill_in "Billing", with: @task.billing
    fill_in "Contact", with: @task.contact
    fill_in "Name", with: @task.name
    fill_in "Payment status", with: @task.payment_status
    fill_in "Priority", with: @task.priority
    fill_in "Remark", with: @task.remark
    fill_in "Service info", with: @task.service_info
    fill_in "Service type", with: @task.service_type_id
    fill_in "Status", with: @task.status
    click_on "Create Task"

    assert_text "Task was successfully created"
    click_on "Back"
  end

  test "updating a Task" do
    visit tasks_url
    click_on "Edit", match: :first

    fill_in "Address line", with: @task.address_line
    fill_in "Area line", with: @task.area_line
    fill_in "Assigned employee", with: @task.assigned_employee_id
    fill_in "Billing", with: @task.billing
    fill_in "Contact", with: @task.contact
    fill_in "Name", with: @task.name
    fill_in "Payment status", with: @task.payment_status
    fill_in "Priority", with: @task.priority
    fill_in "Remark", with: @task.remark
    fill_in "Service info", with: @task.service_info
    fill_in "Service type", with: @task.service_type_id
    fill_in "Status", with: @task.status
    click_on "Update Task"

    assert_text "Task was successfully updated"
    click_on "Back"
  end

  test "destroying a Task" do
    visit tasks_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Task was successfully destroyed"
  end
end
